<?php
class Webgriffe_SetefiMonetaWeb_Helper_Data extends Mage_Core_Helper_Abstract
{
    public static function log()
    {
        $args = func_get_args();
        $formattedMsg = call_user_func_array('sprintf', $args);
        Mage::log($formattedMsg, null, 'Webgriffe_SetefiMonetaWeb.log', Mage::getStoreConfig('payment/wgsetefimw/debug'));
    }

    /**
     * Su versioni precedenti la 1.4.0.1 (compresa), alcune funzionalità
     * non sono utilizzabili, come il Salvataggio Transazioni
     */
    public function isOldVersion()
    {
        return version_compare(Mage::getVersion(), '1.4.0.1', '<=');
    }


}