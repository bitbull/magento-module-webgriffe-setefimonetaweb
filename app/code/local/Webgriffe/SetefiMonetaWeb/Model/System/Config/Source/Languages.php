<?php
class Webgriffe_SetefiMonetaWeb_Model_System_Config_Source_Languages
{

    public function toOptionArray()
    {
        $_langs = array(
            'ITA' => 'Italian',
            'USA' => 'English',
            'SPA' => 'Spanish',
            'FRA' => 'French',
            'DEU' => 'German',
        );

        $options = array();
        $options[] = array(
            'value' => '',
            'label' => Mage::helper('adminhtml')->__('-- Please Select --')
        );
        foreach ($_langs as $code => $label) {
            $options[] = array(
                'value' => $code,
                'label' => Mage::helper('wgsetefimw')->__($label)
            );
        }
        return $options;
    }
}
