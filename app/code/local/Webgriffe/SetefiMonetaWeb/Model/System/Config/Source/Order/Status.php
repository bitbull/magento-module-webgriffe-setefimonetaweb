<?php
/**
 * Order Statuses source model for KeyClient
 */
class Webgriffe_SetefiMonetaWeb_Model_System_Config_Source_Order_Status
{
    const STATE_STATUS_SEPARATOR = '::';

    // set null to enable all possible
    protected $_selectableStates = array(
        Mage_Sales_Model_Order::STATE_NEW,
        Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
        Mage_Sales_Model_Order::STATE_PROCESSING,
        Mage_Sales_Model_Order::STATE_CANCELED,
        Mage_Sales_Model_Order::STATE_HOLDED,
    );

    public function toOptionArray()
    {
        $options = array();

        $options[] = array(
            'value' => '',
            'label' => Mage::helper('adminhtml')->__('-- Please Select --')
        );

        foreach ($this->_selectableStates as $state) {
            $optionGroup = array();
            $statuses = Mage::getModel('sales/order_status')
                ->getCollection()
                ->addStateFilter($state);
            foreach ($statuses as $status) {
                $optionGroup[] = array(
                    'label' => $status->getLabel(),
                    'value' => sprintf('%s%s%s', $state, self::STATE_STATUS_SEPARATOR, $status->getStatus())
                );
            }

            $options[] = array(
                'label' => Mage::getSingleton('sales/order_config')->getStateLabel($state),
                'value' => $optionGroup,
            );
        }

        return $options;
    }
}
