<?php

class Webgriffe_SetefiMonetaWeb_Model_Pay extends Mage_Payment_Model_Method_Abstract
{
    const PAYMENT_TYPE_AUTH = 'AUTHORIZATION';
    const PAYMENT_TYPE_SALE = 'SALE';

    protected $_code        = 'wgsetefimw';
    protected $_responseUrl = 'wgsetefimw/return/response';
    protected $_errorUrl    = 'wgsetefimw/return/error';

    #protected $_paymentUrl			= 'https://www.monetaonline.it:443/MPI2/gateway/payment/payment.jsp';
    #protected $_tokenUrl			= 'https://www.monetaonline.it/MPI2/servlet/PaymentInitHTTPServlet';


    // URL dell’ambiente di PRODUZIONE:
    #protected $_tokenUrl = 'https://www.monetaonline.it/monetaweb/hosted/init/http';

    // URL dell’ambiente di TEST:
    #protected $_tokenUrl			= 'https://test.monetaonline.it/monetaweb/hosted/init/http';

    /**
     * Here you will need to implement authorize, capture and void public methods
     *
     * @see examples of transaction specific public methods such as
     * authorize, capture and void in Mage_Paygate_Model_Authorizenet
     */

    protected function _log()
    {
        $args = func_get_args();
        call_user_func_array(array('Webgriffe_SetefiMonetaWeb_Helper_Data', 'log'), $args);
    }

    /**
     * Get checkout session namespace
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Get current quote
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return $this->getCheckout()->getQuote();
    }

    /**
     * Using internal pages for input payment data
     *
     * @return bool
     */
    public function canUseInternal()
    {
        return false;
    }

    /**
     * Using for multiple shipping address
     *
     * @return bool
     */
    public function canUseForMultishipping()
    {
        return false;
    }

    public function isInitializeNeeded()
    {
        return true;
    }

    public function validate()
    {
        $id = trim($this->getConfigData('merchant_id'));
        $pass = trim($this->getConfigData('merchant_password'));
        $new_order = $this->getConfigData('order_new_state');
        $ko_order = $this->getConfigData('order_ko_state');
        $ok_order = $this->getConfigData('order_ok_state');
        $description = trim($this->getConfigData('description'));

        $this->_log("Called validate() with following parameters:");
        $this->_log("ID='%s'", $id);
        $this->_log("PASS='%s'", $pass);
        $this->_log("NEW ORDER STATE='%s'", $new_order);
        $this->_log("OK ORDER STATE='%s'", $ok_order);
        $this->_log("KO ORDER STATE='%s'", $ko_order);
        $this->_log("DESCRIPTION='%s'", $description);

        if (empty($id) || empty($ko_order) || empty($ok_order) || empty($description)) {
            Mage::throwException(Mage::helper('wgsetefimw')->__('Some configuration parameters are missing for Setefi payment method.'));
        }

        if (strlen($description) > 64) {
            Mage::throwException(Mage::helper('wgsetefimw')->__('The field Description in the configuration can\'t be longer than 64 characters.'));
        }

        return $this;
    }

    public function initialize($paymentAction, $stateObject)
    {
        list($state, $status) = explode(
            Webgriffe_SetefiMonetaWeb_Model_System_Config_Source_Order_Status::STATE_STATUS_SEPARATOR,
            $this->getConfigData('order_new_state')
        );

        $stateObject->setState($state);
        $stateObject->setStatus($status);
        $stateObject->setIsNotified(false);

        // Getting configuration
        $id = $this->getConfigData('merchant_id');
        $description = $this->getConfigData('description');

        //Getting other needed informations
        $orderid = $this->getQuote()->getReservedOrderId();
        $this->_log("Called initialize() for Order '%s'", $orderid);

        if ($this->getQuote()->isVirtual()) {
            $address = $this->getQuote()->getBillingAddress();
        } else {
            $address = $this->getQuote()->getShippingAddress();
        }

        //converting to EURO
        $baseAmount = $address->getBaseGrandTotal();
        $storeCurrency = Mage::getSingleton('directory/currency')->load($this->getQuote()->getBaseCurrencyCode());
        $amount = $storeCurrency->convert($baseAmount, 'EUR');
        $this->_log("Order base amount is %s %d corresponding to EUR %d", $storeCurrency->getCode(), $baseAmount, $amount);

        $amt = number_format($amount, 2, '.', '');
        $id = $this->getConfigData('merchant_id');
        $password = $this->getConfigData('merchant_password');
        $langid = $this->getConfigData('language');
        $udf1 = urlencode($this->getConfigData('description'));
        $action = "4";
        $currencycode = "978";
        $responseurl = urlencode(Mage::getUrl($this->_responseUrl, array('order_id' => $orderid)));
        $errorurl = urlencode(Mage::getUrl($this->_errorUrl, array('order_id' => $orderid)));
        $trackid = $orderid;

        $data = "id=$id&password=$password&action=$action&amt=$amt&currencycode=$currencycode&langid=$langid&responseurl=$responseurl&errorurl=$errorurl&trackid=$trackid&udf1=$udf1";
        $this->_log("HTTP Query String that will be sent: %s", $data);

        $testMode = $this->getConfigData('testmode');
        if ($testMode) {
            $paymentrequesturl = trim($this->getConfigData('url_test'));
        } else {
            $paymentrequesturl = trim($this->getConfigData('url_prod'));
        }
        $this->_log("Test mode: %s - Payment Request URL: %s", $testMode, $paymentrequesturl);

        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $paymentrequesturl);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curlHandle, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');     //Suggerito da Setefi
        try {
            $buffer = curl_exec($curlHandle);
            $curlErrors = curl_error($curlHandle);
            curl_close($curlHandle);
            if ($curlErrors) {
                throw new Exception($curlErrors);
            }
        } catch (Exception $ex) {
            $this->_log("Error while performing curl request: ".$ex->getMessage());
            throw $ex;
        }
        $this->_log("CURL result: %s", $buffer);

        $token = explode(":", $buffer, 2);
        $tid = $token[0];
        $paymenturl = $token[1];
        $this->_log("Token ID: %s", $tid);
        $this->_log("Payment URL: %s", $paymenturl);

        Mage::getSingleton('wgsetefimw/session')->setPaymentUrl($paymenturl);
        Mage::getSingleton('wgsetefimw/session')->setPaymentId($tid);
        Mage::getSingleton('wgsetefimw/session')->setOrderId($orderid);

        $this->_log("Session Obj: %s", print_r(Mage::getSingleton('wgsetefimw/session')->debug(), true));

        return $this;
    }

    public function getOrderPlaceRedirectUrl()
    {
        //return Mage::getUrl('wgsetefimw/return/bankpost');
        $url = Mage::getSingleton('wgsetefimw/session')->getPaymentUrl();
        $id = Mage::getSingleton('wgsetefimw/session')->getPaymentId();

        Mage::getSingleton('wgsetefimw/session')->unsPaymentUrl();
        $this->_log("Session Obj: %s", print_r(Mage::getSingleton('wgsetefimw/session')->debug(), true));

        $redirectUrl = sprintf("%s?PaymentID=%s", $url, $id);
        $this->_log("Redirect URL: %s", $redirectUrl);
        return $redirectUrl;
    }
}
