<?php
class Webgriffe_SetefiMonetaWeb_ReturnController extends Mage_Core_Controller_Front_Action
{
    protected function _log()
    {
        $args = func_get_args();
        call_user_func_array(array('Webgriffe_SetefiMonetaWeb_Helper_Data', 'log'), $args);
    }

    protected function _getTransaction(Mage_Sales_Model_Order $order, $transactionId, $returnedParams)
    {
        $transaction = null;
        $payment = $order->getPayment();
        if ($payment->getId()) {
            $transaction = Mage::getModel('sales/order_payment_transaction')
                ->setOrderId($order->getId())
                ->setPaymentId($payment->getId())
                ->setTxnId($transactionId)
                ->setOrderPaymentObject($payment)
                ->setAdditionalInformation(
                    Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, $returnedParams)
                ->setCreatedAt(Varien_Date::now());
        }
        return $transaction;
    }

    protected function _createOrderTransaction($order, $transactionId, $returnedParams, $isSuccessful)
    {
        $this->_log('Called createOrderTransaction()');

        if (Mage::helper('wgsetefimw')->isOldVersion()) {
            $this->_log("Transactions aren't supported on Magento versions <= 1.4.0.1; current version is: %s", Mage::getVersion());
            return;
        }

        $paymentMethod = $order->getPayment()->getMethodInstance();
        $transaction = $this->_getTransaction($order, $transactionId, $returnedParams);
        try {
            $transactionType = Mage_Sales_Model_Order_Payment_Transaction::TYPE_ORDER;
            if ($returnedParams['responsecode'] == '00') {
                $transactionType = Mage_Sales_Model_Order_Payment_Transaction::TYPE_PAYMENT;
            } else if ($returnedParams['responsecode'] == '000') {
                $transactionType = Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH;
            }

            $transaction = $transaction
                ->setIsClosed($isSuccessful)
                ->setTxnType($transactionType)
                ->save();
            $this->_log("Transaction was created with id: %s", $transaction->getId());
        } catch (Exception $transactionSaveException) {
            $this->_log($transactionSaveException->getMessage());
        }
    }

    private function __ko($orderId = null)
    {
        $this->_log("Called __ko() on ReturnController for Order '%s'", $orderId);
        if (empty($orderId)) {
            $orderId = Mage::getSingleton('wgsetefimw/session')->getOrderId();
            $this->_log("Retrieved Order Id '%s' from Session", $orderId);
        }
        if (!empty($orderId)) {
            $this->_log("Processing __ko()");
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);

            Mage::getSingleton('wgsetefimw/session')->unsOrderId();
            Mage::getSingleton('wgsetefimw/session')->unsPaymentId();

            list($state, $status) = explode(
                Webgriffe_SetefiMonetaWeb_Model_System_Config_Source_Order_Status::STATE_STATUS_SEPARATOR,
                Mage::getModel('wgsetefimw/pay')->getConfigData('order_ko_state')
            );

            if (!strcmp(Mage_Sales_Model_Order::STATE_CANCELED, $state)) {
                $order->cancel()->save();
                $this->_log("Order %s canceled", $order->getIncrementId());

                if (Mage::helper('core')->isModuleEnabled('Enterprise_Reward')) {
                    if (Mage::getModel('wgsetefimw/pay')->getConfigData('restore_points')) {
                        $this->_log("Should restore %d reward points for Order '%s'", $order->getRewardPointsBalance(),  $orderId);
                        $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
                        $order->setCustomer($customer);
                        $observer = new Varien_Event_Observer();
                        $event = new Varien_Event();
                        $event->setOrder($order);
                        $observer->setEvent($event);
                        try {
                            Mage::getSingleton('enterprise_reward/observer')
                                ->revertRewardPoints($observer);

                            $websiteId = Mage::app()->getStore($order->getStoreId())->getWebsiteId();
                            /** @var Enterprise_Reward_Model_Reward $rewardPoints */
                            $rewardPoints =
                                Mage::getModel('enterprise_reward/reward')
                                    ->setCustomerId($order->getCustomer()->getId())
                                    ->setWebsiteId($websiteId)
                                    ->loadByCustomer();
                            $balance = $rewardPoints->getPointsBalance();
                            $this->_log("Reward points for Order '%s' restored, customer new balance is %d", $orderId, $balance);
                        } catch (Exception $e) {
                            $this->_log("An error occurred trying to restore reward points: '%s'", $e->getMessage());
                        }
                    }
                }
            } else {
                $message = $this->__('The payment has not been authorized from Setefi payment gateway.');
                $order
                    ->setState($state, $status, $message)
                    ->save();
                $this->_log(
                    "Order %s state set to '%s', status set to '%s'",
                    $order->getIncrementId(),
                    $state,
                    $status
                );
            }
        } else {
            $this->_log("Already processed __ko()");
        }
    }

    public function errorAction()
    {
        $params = $this->getRequest()->getParams();
        $this->_log("Called errorAction() with following parameters: %s", print_r($params, true));
        $orderId = null;
        if (isset($params['trackid'])) {
            $orderId = $params['trackid'];
        } else if ((isset($params['order_id']))) {
            $orderId = $params['order_id'];
        }
        $this->__ko($orderId);
        $this->_redirect('checkout/onepage/failure');
    }

    public function responseAction()
    {
        $this->_log("Session Obj: %s", print_r(Mage::getSingleton('wgsetefimw/session')->debug(), true));

        $params = $this->getRequest()->getParams();
        $this->_log("Called responseAction() with following parameters: %s", print_r($params, true));

        $result = $params['result'];

        // Pagamento annullato dall'utente
        if (!strcasecmp('CANCELED', $result)
            || (strcasecmp('APPROVED', $result) && strcasecmp('CAPTURED', $result))
        ) {
            $orderId = Mage::getSingleton('wgsetefimw/session')->getOrderId();
            if (empty($orderId)) {
                $orderId = $this->getRequest()->getParam('order_id');
            }
            if (!strcasecmp('CANCELED', $result)) {
                $this->_log("Payment for Order %s explicitly canceled by the User", $orderId);
            }
            $this->__ko($orderId);
            #$this->_redirect('checkout/onepage/failure');
            echo "REDIRECT=" . Mage::getUrl('checkout/onepage/failure');
        } else {
            $paymentID = $params['paymentid'];
            $auth = $params['auth'];
            $ref = $params['ref'];
            $tranid = $params['tranid'];
            $trackid = $params['trackid'];
            $details = $params['udf1'];
            $responsecode = $params['responsecode'];

            list($state, $status) = explode(
                Webgriffe_SetefiMonetaWeb_Model_System_Config_Source_Order_Status::STATE_STATUS_SEPARATOR,
                Mage::getModel('wgsetefimw/pay')->getConfigData('order_ok_state')
            );

            $order = Mage::getModel('sales/order')->loadByIncrementId($trackid);

            if (!strcmp(Mage_Sales_Model_Order::STATE_CANCELED, $state)) {
                $order->cancel()->save();
                $this->_log("Warning: Order %s canceled upon approved payment!", $order->getIncrementId());
                $this->_createOrderTransaction($order, $tranid, $params, false);
            } else {
                $message = sprintf(
                    "Payment authorized by Setefi payment gateway; AUTH CODE: %s - OP REF: %s",
                    $auth,
                    $tranid . '_' . $ref . '_' . $details . '_' . $result
                );
                $this->_log($message);
                $order
                    ->setState($state, $status, $message)
                    ->sendNewOrderEmail()
                    ->save();
                $this->_log("Order %s state set to '%s', status set to '%s'", $trackid, $state, $status);
                $this->_createOrderTransaction($order, $tranid, $params, true);
            }
            echo "REDIRECT=" . Mage::getUrl('checkout/onepage/success');
        }
    }

    /*
	public function bankpostAction() {
		$url = Mage::getSingleton('wgsetefimw/session')->getPaymentUrl();
		$id = Mage::getSingleton('wgsetefimw/session')->getPaymentId();
        $this->_log("Called bankpostAction()");
        $this->_log("Payment URL: %s", $url.'?PaymentID='.$id);

		Mage::getSingleton('wgsetefimw/session')->unsPaymentUrl();
				
		echo "
			<html>
			<head>
				<title>".$this->__('Setefi Payment Gateway')."</title>
				<script type=\"text/javascript\">					
					function redirect() {
						document.setefiform.submit();
					}
				</script>				
			</head>
			<body style=\"text-align:center; font-family:Arial; font-size:14px; font-weight:bold;\" onload=\"javascript:setTimeout('redirect()', 3000);\">
				<br/>
				<form action=\"$url\" method=\"GET\" id=\"setefiform\" name=\"setefiform\">
					<input type=\"hidden\" name=\"PaymentID\" value=\"$id\" />
															
					".$this->__('You will be redirected to the Setefi payment page in a few seconds').".
					<br/><br/>
					".$this->__('If you are not redirected soon').": 
					<input type=\"submit\" style=\"background:none; text-decoration:underline; border:none; font-weight:bold; color:#2200CC; cursor:pointer;\" value=\"".$this->__('click here')."\" />
				</form>		
			</body>
			</html>
		";		
	}
    */
}